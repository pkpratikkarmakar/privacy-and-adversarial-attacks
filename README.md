# Privacy and adversarial attacks

In an effort to find upper and lower bounds of privacy and adversarial advantage, I am working inititally with basic MNIST dataset. The idea is to design a cost function which includes a information leakage component. The cost function as a whole is to be minimized w.r.t. the attack model parameters, and has to be maximized w.r.t. the data that we send into the attack and defence models as queries.
Say, the victim model is $`f`$ and the attack model is $`f'`$. We design a cost function as given below:
```math
L(\theta,Q)=l(f(Q),f'(Q))+\alpha * R1(Q)+ \beta * R2(f')
```
where $`R1`$ and $`R2`$ are regularisers on data ($`Q`$) and model parameters of $`f'`$ respectively. Here $`Q`$ is being generated as a convex combination of $`Q_0`$, which is the basis of the dataset. We are trying to minimize this $`L`$ w.r.t. the parameters of $`f'`$ and maximize w.r.t. $`Q`$, i.e. the query. The initial work show a smooth descend in training curve with asymptotic nature. 

Further updates will be given in this repository.
